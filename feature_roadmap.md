# Discord integration
https://discordpy.readthedocs.io/en/latest/api.html

https://www.npmjs.com/package/discord.js

https://serverspace.us/support/help/create-discord-bot-windows-server-2019/

Discord app link: https://discord.com/developers/applications/1189008304156975114/information

# eye Tracking?
    - probably not reliable enough.

# Enhanced Editing
- highlight words via command
- insert new text into highlighted text
- select corrections via a dropdowninsert new text into highlighted text
- improved voice editing, seamless NOT via button

# Predictive Text / Spell mode
- Voice controlled predictive text similar to phones
- https://medium.com/analytics-vidhya/build-a-simple-predictive-keyboard-using-python-and-keras-b78d3c88cffb

# Command & Control
#### Local language model (Speech=>Text)
- Emily Shea voice controlled development: https://www.youtube.com/watch?v=YKuRkGkf5HU&t=431s
- https://huggingface.co/docs/transformers/model_doc/speech_to_text_2
- https://www.assemblyai.com/blog/end-to-end-speech-recognition-pytorch/

###### Custom Voice Training
- let users put training data into folders. 1 folder per command and 1 folder per letter.
- write a facility to automate the recording of training data

# VAD
- Voice Activity Detection in Python. ie 'wakeup'.
- https://pypi.org/project/webrtcvad/
- Automatic stopping as well? Either let deepgram do it or let our VAD thing determine? no need to let go of the 'record' key

#### Text=>Action 
- fuzzy matching
- chatbot libraries
- NLTK, stemming, lemmatization
- research other software/libraries for Command&Control


# Hardware Support
- HID based
- GUI configurable hardware setup - similar to what emulators do!
- map hardware inputs => keyboard shortcuts
- 'command' hotkey vs 'transcribe' hotkey
- map custom code/scripts into HID
- custom scripting language?
- bluetooth?

# Editing Any Textbox (dragon-style)
- have to somehow hook into the active window and active textbox?
- Microsoft spell check API? Looks like its not available in python?

## GUI
- choose mic from dropdown
- choose static/live from dropdown?
- choose voice service from dropdown?
- autoresize textedit box

## Phone App

## Phone Keyboard App

## statistical/SLP focused features
