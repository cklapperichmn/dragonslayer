import re


def parse_commands(text: str, commandlist: str, escapeword: str) -> str:
    """
    Applies the commandlist in settings.yaml to the text, returns processed text
    :param text: str, transcription from a voice service
    :return: parsed transcription
    """
    for cmd in commandlist:
        pattern, result = cmd
        findpattern_avoidescapeword = f"(?<!({escapeword }))({pattern})(?![a-z])"
        find_escape_plus_pattern = f"{escapeword} {pattern}"
        text = re.sub(findpattern_avoidescapeword, result, text, flags=re.IGNORECASE)
        text = re.sub(find_escape_plus_pattern, result, text, flags=re.IGNORECASE)
    return text


def capitalize(s: str) -> str:
    """
    :param s:
    :return:
    """
    if not s: return ''
    find_lowercase = r"((?<=\. )[a-z])|( i )"
    matches = re.finditer(find_lowercase, s)
    for match in matches:
        start, end = match.span()
        s = s[:start] + s[start:end].upper() + s[end:]
    s = s[0].upper() + s[1:]
    return s


def fix_spaces_around_punctuation(text: str) -> str:
    """
    :param text:
    :return:
    """
    if not text: return ''

    text = re.sub(' \)', ')', text)
    text = re.sub('\( ', '(', text)

    for p in [r'.', r'?']:
        text = re.sub(fr'\{p}(?!= )', f'{p} ', text)  # if punctuation is not followed by space, insert a space
        text = re.sub(fr' \{p}',      f'{p}', text)  # if there is an extra space, remove it

    for p in ['!', ',']:
        text = re.sub(f'{p}(?!= )', f'{p} ', text)  # if punctuation is not followed by space, insert a space
        text = re.sub(f' {p}', f'{p}', text)  # if there is an extra space, remove it

    # match and replace quotes
    find_quotes = r' ?" ?'
    odd = True
    offset = 0
    for match in re.finditer(find_quotes, text):
        if odd: replace = ' "'
        else:   replace = '" '
        start, end = match.span()
        text = text[:start+offset] + replace + text[end+offset:]
        offset += len(replace) - len(match.group())
        odd = not odd

    # # removing leading/trailing space
    # if text and text[0] == ' ':
    #     text = text[1:]
    # if text and text[-1] == ' ':
    #     text = text[:-1]

    text = re.sub(' +', ' ', text) # remove duplicate spaces
    return text


def add_closing_punctuation(s: str) -> str:
    if s[-1] not in ['.', '!', '?']:
        s += '.'
    return s


if __name__ == '__main__':
    s = "hello. this is a test sentence i didn't capitalize"
    s_cap = capitalize(s)
    print(s_cap)

    s = '"." test " remove uneeded spaces . " This sentence has weird stuff ? !'
    s = fix_spaces_around_punctuation(s)
    print(s)
