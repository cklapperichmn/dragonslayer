import asyncio
import datetime
import threading
import traceback
from typing import Callable
import keyboard
import pyperclip
import yaml
import textprocessing
import gui_pyside6
from api_keys import get_deepgram_api_key
from gui_pyside6 import DragonSlayerBar, invoke_in_main_thread
import hotkey_activator
from transcriber_deepgram import transcriber_deepgram
from transcriber_assemblyai import transcriber_assemblyai
from transcriber_revai import transcriber_revai
from recordingdevice_pyaudio import recordingdevice
from transcriber import Transcriber
import logging



def get_update_textbox_callback(dragonbar, transcriber_funcs: Transcriber, method='gui'):
    """
    :param dragonbar: object representing the GUI
    :param recorder: object representing a recording device
    :param method: 'gui' or 'vkeyboard'
    :return: update_textbox() callback, to be called anytime new dictation text is available
    """
    prevtext=''
    def write_to_virtualkeyboard(response):
        # disable mouse and keyboard for duration of callback?
        nonlocal prevtext

        newtext = transcriber_funcs.response_to_transcript(response)
        isfinal = transcriber_funcs.response_is_final(response)

        if not newtext: return

        newtext = textprocessing.parse_commands(text=newtext, commandlist=settings['commandlist'],
                                                escapeword=settings['command-escapeword'])
        newtext = textprocessing.fix_spaces_around_punctuation(newtext)
        newtext = textprocessing.capitalize(newtext)
        if newtext[-1] != ' ': newtext += ' '

        for c in prevtext:
            keyboard.press_and_release('backspace')
        for c in newtext:
            keyboard.press_and_release(c)

        if isfinal:
            prevtext = ''
        else:
            prevtext = newtext

    def write_to_gui(response):
        def process_full_textbox(text):
            text = textprocessing.fix_spaces_around_punctuation(text)
            text = textprocessing.capitalize(text)
            return text

        newtext = transcriber_funcs.response_to_transcript(response)
        if not newtext: return

        newtext = textprocessing.parse_commands(text=newtext, commandlist=settings['commandlist'],
                                                escapeword=settings['command-escapeword'])
        if newtext[-1] != ' ': newtext += ' '
        dragonbar.insert_unfinished_text(newtext)

        if transcriber_funcs.response_is_final(response):
            dragonbar.finalize_text(textprocessing_callback=process_full_textbox)

    if method=='gui': return write_to_gui
    elif method=='vkeyboard': return write_to_virtualkeyboard
    else: raise ValueError


def get_recording_stopstart_callbacks(dragonbar: DragonSlayerBar, recorder: recordingdevice,
                                      transcriber_funcs: Transcriber, get_api_key: Callable,
                                      on_recordkey_tapped: Callable) -> (Callable, Callable):
    """
    :param dragonbar: object representing the GUI
    :param recorder: object representing a recording device
    :param transcriber_funcs:
    :param get_api_key: Callable, takes no args
    :param on_recordkey_tapped: perform when the recording key is 'tapped'. Takes a dragonbar object as input, returns nothing
    :return: startaudio() and stopaudio() functions
    """
    update_textbox_callback = get_update_textbox_callback(dragonbar, transcriber_funcs)

    def record() -> None:
        try:
            live_transcription_queue, vad_queue = recorder.startaudio(n=2)
        except Exception as e:
            print("Error Initializing Microphone.")
            timestr = str(datetime.datetime.now())
            exception_message = traceback.format_exception(e)
            msg = timestr + '\n' + ''.join(exception_message)
            logging.error(msg)
            return None

        returnvalues = {'error_code': None,
                        'message': 'NOT SET',
                        'traceback': 'TRACEBACK NOT SET'}

        awaitable = transcriber_funcs.transcribe_live(lambda: live_transcription_queue.get(), api_key=get_api_key(),
                                       channels=1, samplerate=recorder.get_samplerate(), returnvalues=returnvalues,
                                       callback=lambda response: invoke_in_main_thread(update_textbox_callback,
                                                                                       response))

        transcription_thread = threading.Thread(target=asyncio.run, args=(awaitable,))
        transcription_thread.start()

        dragonbar.show_recording_started()

        transcription_thread.join()

        recorder.stopaudio()  # just in case it wasn't already stopped by stop_recording()
        dragonbar.show_recording_stopped()

        """
        Error handling on Failed Transcription
        """
        if returnvalues.get('error_code'):
            print("Error Connecting to Service.")
            timestr = str(datetime.datetime.now())
            msg = timestr + '\n' + returnvalues['traceback']
            logging.error(msg)
            print(returnvalues['suggestion'])
            return None

    def start_recording() -> None:
        if dragonbar.is_disabled:
            print(dragonbar.is_disabled)
            return

        t = threading.Thread(target=record)
        t.start()

    def stop_recording() -> None:
        if dragonbar.is_disabled:
            return

        if recorder.is_recording and recorder.get_duration_seconds()>0.25:
            on_recordkey_tapped()
        recorder.stopaudio()

    return start_recording, stop_recording


def copypaste_on_tap(dragonbar):
    text = dragonbar.get_current_text()
    pyperclip.copy(text)
    keyboard.press_and_release('ctrl+v')
    dragonbar.clear_text_window()

def dummy_tap_command_func(dragonbar):
    pass


if __name__ == '__main__':
    assembly_transcriber = transcriber_assemblyai
    revai_transcriber = transcriber_revai
    dg_transcriber = transcriber_deepgram

    logging.basicConfig(filename='errorlog.txt')

    with open('settings.yaml', 'r') as fp:
        settings = yaml.safe_load(fp)
    dragonbar = DragonSlayerBar(width=500, commandbarheight=50, textwindowheight=60,
                                stayontop=settings['keep-window-on-top'])

    recorder = recordingdevice(channels=1)

    if settings.get('micname') and settings['micname'] != 'default':
        recorder.set_audio_device_by_name(settings['micname'])

    def microphone_changed(device_id: int) -> None:
        assert(not recorder.is_recording)
        micname = recorder.get_deviceinfo()[device_id]['name']
        dragonbar.set_recordbutton_tooltip(micname)
        recorder.set_audio_device_by_id(device_id)

    start_recording, stop_recording = get_recording_stopstart_callbacks(dragonbar, recorder, dg_transcriber,
                                                                        get_deepgram_api_key, copypaste_on_tap)
    dragonbar.set_callback('start_recording', start_recording)
    dragonbar.set_callback('stop_recording', stop_recording)


    def list_microphones():
        """ must return a list of deviceinfo dicts with keys (index, name)"""
        return recorder.get_deviceinfo()

    dragonbar.set_callback('microphone_changed', microphone_changed)
    dragonbar.set_callback('list_microphones', list_microphones)
    dragonbar.set_callback('list_transcription_providers', lambda: ["Deepgram.ai"])
    dragonbar.set_callback('list_tap_commands', lambda: ["None", "Copy+Paste+Clear"])
    dragonbar.set_callback('static_transcribe_func', lambda x: dg_transcriber.transcribe_static(x, api_key=get_deepgram_api_key()))

    hotkey_activator.set_hotkey_bindings(settings['recording_hotkey'],
                                         onpress=start_recording,
                                         onrelease=stop_recording
                                         )
    gui_pyside6.launch_gui()
