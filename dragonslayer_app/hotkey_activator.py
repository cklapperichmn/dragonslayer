import time
from threading import Lock
import keyboard
from typing import Callable


def set_hotkey_bindings(hotkey: str = 'ctrl+space', onpress: Callable = lambda: 0, onrelease: Callable = lambda: 0) -> None:
    """
    binds onpress and onrelease to the hotkey listed
    :param onpress: callable[None]
    :param onrelease: callable[None]
    :param hotkey: string, keys separated by '+' ie 'ctrl+space+a'
    :return: None
    """
    hotkeypressed = False
    hotkeylist = hotkey.split('+')

    def onpress_internal():
        nonlocal hotkeypressed
        if not hotkeypressed:
            hotkeypressed = True
            onpress()

    def onrelease_internal(key):
        nonlocal hotkeypressed
        if (key.name in hotkeylist) and hotkeypressed:
            hotkeypressed = False
            onrelease()

    keyboard.add_hotkey(hotkey, onpress_internal)
    keyboard.on_release(onrelease_internal)


if __name__ == '__main__':
    def onpress():
        print('hi')
        time.sleep(3)

    def onrelease():
        print('release')

    set_hotkey_bindings(onpress, onrelease, 'esc')
    keyboard.wait()
