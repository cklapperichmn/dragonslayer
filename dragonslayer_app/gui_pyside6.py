import asyncio
import os
import sys
from typing import Callable, List
import PySide6.QtWidgets
from PySide6 import QtCore
from PySide6.QtCore import QSize, QPoint, QTimer
from PySide6.QtGui import QIcon, QFontMetrics, QWindow, QPalette, QColor, Qt, QTextOption
from PySide6.QtWidgets import QApplication, QWidget, QPushButton, QMainWindow, QTextEdit, QGridLayout, QGroupBox, \
    QVBoxLayout, QHBoxLayout, QComboBox, QMdiSubWindow, QFormLayout, QLabel

class RecordButton(QPushButton):
    def __init__(self, size):
        super().__init__()

        self.icon_rec_green = QIcon('assets/mic-green.png')
        self.icon_rec_red = QIcon('assets/mic-red.png')
        self.setFixedSize(QtCore.QSize(size,size))
        self.setIconSize( QtCore.QSize(size,size))

        self.is_recording = False
        self.setIcon(self.icon_rec_red)

    def switch_state(self) -> None:
        self.off() if self.is_recording else self.on()

    def on(self) -> None:
        self.is_recording = True
        self.setIcon(self.icon_rec_green)

    def off(self) -> None:
        self.is_recording = False
        self.setIcon(self.icon_rec_red)


class DragonTextBox(QGroupBox):
    """
    "Awareness" - of the current selection of text OR text cursor
    select a word *for* the user - move the cursor, and highlight text automatically

    edit text programatically
    call into predict (via callbacks)
    "on textbox changed" callback
    print statements to debug cursor position

    ---------------------

    toPlainText ()
    textCursor()

    SIGNALS
    self.textchanged
    self.selectionchanged
    "A slot is called when a signal connected to it is emitted."

    Interface
    Bar below text box
    TAB - insert current suggestion, move cursor to the right of the current word
    RIGHT - move suggestion right
    LEFT - move suggestion left
    """

    def __generate_suggestion_html(self, text: str, bold: bool = False):
        # https://doc.qt.io/qtforpython/overviews/richtext-html-subset.html#supported-html-subset
        html_text = fr"{text}"

        if bold:
            html_text += "<b>" + html_text + r"<\b>"
        html_text = r"<b>" + text + r"<\b>"

        return html_text

    def __get_suggestion_label(self, text: str = "", bold: bool = False):
        label = QPushButton()
        label.setText(text)
        # label.setTextFormat(Qt.RichText)
        # html = self.__generate_suggestion_html(text, bold=bold)
        # label.setText(html)
        return label

    def set_suggestions(self, words: List):
        for word, label in zip(words, self.suggestion_labels):
            # html = self.__generate_suggestion_html(word, bold=False)
            label.setText(word)
        return None

    def __init__(self, n_suggestions=5):
        self.n_suggestions = n_suggestions
        self.suggestion_labels = []
        self.suggestion_median = n_suggestions//2 + 1

        super().__init__()

        # build the textbox
        self.textbox = QTextEdit()
        t = self.textbox
        t.setWordWrapMode(QTextOption.WrapMode.WordWrap)
        t.setLineWrapMode(t.LineWrapMode.WidgetWidth)
        t.font = t.document().defaultFont()
        t.fontMetrics = QFontMetrics(t.font)

        # Layout - suggestion box
        self.suggestionbox = QGroupBox()
        suggestionbox_layout = QGridLayout()
        self.suggestionbox.setLayout(suggestionbox_layout)

        self.suggestion_labels = [self.__get_suggestion_label('') for _ in range(self.n_suggestions)]
        for i, label in enumerate(self.suggestion_labels):
            suggestionbox_layout.addWidget(label, 0, i)
        self.set_suggestions(['do','re','mi','fa','so'])

        # layout - Main
        layout = QGridLayout()
        layout.addWidget(self.textbox, 0, 0)
        layout.addWidget(self.suggestionbox, 1, 0)
        self.setLayout(layout)

        # awareness variables
        self.workingtext = ""
        self.currentword = ""

    def resize_to_text(self) -> (int, int):
        """
        Resizes the textbox to fit its text, and returns the new width/height of the textbox
        :return: width, height
        """
        textSize = self.textbox.fontMetrics.size(0, self.textbox.toPlainText())
        textWidth = textSize.width() + 30  # constant may need to be tweaked
        textHeight = textSize.height() + 30  # constant may need to be tweaked
        self.textbox.resize(textWidth, textHeight)  # good if you want this to be standalone
        return textWidth, textHeight

    def insert_unfinished_text(self, newtext: str) -> None:
        """
        Deletes text from the previous call, replaces with newtext
        call finalize() to make the text 'stick'.
        :param newtext: str; text to be inserted
        :return:
        """
        for i in range(len(self.workingtext)):
            self.textbox.textCursor().deletePreviousChar()
        self.textbox.textCursor().insertText(newtext)
        self.workingtext = newtext

    def finalize(self, textprocessing_callback: Callable = lambda s: s) -> None:
        """
        Mark the last call to insert_unfinished_text as final and done.
        Then performs text processing on the entire contents of the ntextbox.
        The next call to insert_unfinished_text after finalize() will not delete *anything*.
        :param textprocessing_callback: function to process/edit the entire block of text for grammar, spelling, spacing, etc.
        :return: None
        """
        cursor = self.textbox.textCursor()
        pos = cursor.positionInBlock()
        cursor.setPosition(pos)

        text = self.textbox.toPlainText()
        text = textprocessing_callback(text)
        self.textbox.setText(text)
        self.textbox.setTextCursor(cursor)
        # self.resize_to_text()
        self.workingtext = ""



class SettingsWindow(QMainWindow):
    def __init__(self, callbacks: dict = None, parent=None, width=500, height=500):
        super().__init__(parent=parent)
        self.parentwindow = parent
        self.callbacks = callbacks

        self.setFixedWidth(width)
        self.setFixedHeight(height)
        self.setWindowTitle("SETTINGS")
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)

        miclabel = QLabel("Set Microphone")
        self.mic_select = QComboBox()

        provider_label = QLabel("Select Provider")
        self.provider_select = QComboBox()

        tapcommand_label = QLabel("TapCommand")
        self.tapcommand_select = QComboBox()

        layout = QFormLayout(self)
        layout.addRow(miclabel, self.mic_select)
        layout.addRow(provider_label, self.provider_select)
        layout.addRow(tapcommand_label, self.tapcommand_select)
        qform_box = QGroupBox()
        qform_box.setLayout(layout)

        parentgridlayout = QGridLayout()
        parentgridlayout.addWidget(qform_box, 0, 0)
        parentgridlayout.addWidget(QPushButton("ACCEPT"), 1, 0)

        parentbox = QGroupBox()
        parentbox.setLayout(parentgridlayout)
        self.setCentralWidget(parentbox)

    def set_dictation_provider_names(self, names: list[str]) -> None:
        self.provider_select.addItems(names)

    def closeEvent(self, event:PySide6.QtGui.QCloseEvent) -> None:
        self.parentwindow.setDisabled(False)
        self.parentwindow.is_disabled = False
        self.close()

    def set_microphone(self, name):
        idx = self.mic_select.findData(name)
        self.mic_select.setCurrentIndex(idx)

    def launch_window(self):
        self.callbacks['stop_recording']()

        name_list = [str(d['index']) + '-' + d['name'] for d in self.callbacks['list_microphones']()]
        self.mic_select.clear()
        self.mic_select.addItems(name_list)

        self.tapcommand_select.clear()
        self.tapcommand_select.addItems(self.callbacks['list_tap_commands']())

        self.provider_select.clear()
        self.provider_select.addItems(self.callbacks['list_transcription_providers']())

        self.move(self.parentwindow.x(), self.parentwindow.y()+50)
        self.parentwindow.setDisabled(True)  # also disables child window
        self.setDisabled(False)
        self.show()


    def save_settings(self, settings):
        settings = settings.read_settings()
        
        self.mic_select.currentIndex()

def show_filedialogue(initialdir):
    import tkinter as tk
    from tkinter import filedialog
    root = tk.Tk()
    root.withdraw()
    root.wm_attributes('-topmost', 1)
    filetypes = [('mp3', '*.mp3'), ('mp4', '*.mp4'), ('m4a','*.m4a'),('wav','*.wav')]
    file_path = filedialog.askopenfilename(initialdir=initialdir, filetypes=filetypes, parent=root)
    return file_path

def transcribe_static(transcribe_func):
    defaultdir = None
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    path = show_filedialogue(defaultdir)
    if path:
        with open(path, 'rb') as audio:
            response_dict = asyncio.run(transcribe_func(audio))
        print(response_dict)
        transcript = response_dict['results']['channels'][0]['alternatives'][0]['transcript']
        print(transcript)
        filename = os.path.basename(path).split('.')[0] + '.txt'
        saveto = filename
        with open(saveto,'w+') as fp:
            fp.write(transcript)
    else:
        print("No path provided! closing...")

class DragonSlayerBar(QMainWindow):
    def __init__(self, width=1000, commandbarheight=50, textwindowheight=60, stayontop=False):
        super().__init__()
        self.width = width
        self.buttonheight = commandbarheight
        self.singlelineheight = textwindowheight
        self.height = self.buttonheight + self.singlelineheight
        self.oldPos = QPoint(0,0)
        self.is_disabled = False
        self.callbacks = {}
        self.setWindowFlags(Qt.CustomizeWindowHint)
        if stayontop: self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)

        self.setWindowTitle("Dragonslayer")

        # INIT UI ELEMENTS
        self.textbox = DragonTextBox()

        self.recordbutton = RecordButton(size=self.buttonheight)
        self.recordbutton.clicked.connect(self.handleButton)

        self.closebutton = QPushButton("X")
        self.closebutton.setMaximumSize(30,30)
        self.closebutton.clicked.connect(lambda: self.close())

        self.settingswindow = SettingsWindow(parent=self, width=500, height=500, callbacks=self.callbacks)

        self.gearbutton = QPushButton()
        gearicon = QIcon('assets/gearicon.webp')
        self.gearbutton.setIcon(gearicon)
        self.gearbutton.setMaximumSize(30,30)
        self.gearbutton.clicked.connect(self.settingswindow.launch_window)

        self.pencilbutton = QPushButton("T")
        pencilicon = QIcon('asserts/pencil-32.png')
        self.pencilbutton.setIcon(pencilicon)
        self.pencilbutton.setMaximumSize(32,32)
        self.pencilbutton.clicked.connect(lambda: transcribe_static(transcribe_func=self.callbacks['static_transcribe_func']))

        # LAYOUT
        self.commandbarbox = QGroupBox()
        self.commandbarbox.setContentsMargins(0,0,0,0)
        self.commandbarboxlayout = QHBoxLayout()
        self.commandbarboxlayout.setContentsMargins(0,0,0,0)

        self.commandbarboxlayout.addWidget(self.recordbutton, alignment=QtCore.Qt.AlignCenter)
        self.commandbarboxlayout.addWidget(self.gearbutton, alignment=QtCore.Qt.AlignRight&QtCore.Qt.AlignTop)
        self.commandbarboxlayout.addWidget(self.pencilbutton, alignment=QtCore.Qt.AlignRight&QtCore.Qt.AlignTop)
        self.commandbarboxlayout.addWidget(self.closebutton, alignment=QtCore.Qt.AlignRight&QtCore.Qt.AlignTop)

        self.commandbarbox.setLayout(self.commandbarboxlayout)

        self.parentbox = QGroupBox()
        self.parentbox.setContentsMargins(0,0,0,0)
        self.parentgrid = QVBoxLayout()
        self.parentgrid.setContentsMargins(0,0,0,0)

        self.parentgrid.addWidget(self.commandbarbox)
        self.parentgrid.addWidget(self.textbox)
        self.parentbox.setLayout(self.parentgrid)
        self.setCentralWidget(self.parentbox)

        # Window Sizing
        self.setBaseSize(QSize(self.width, self.height))
        QTimer.singleShot(1, self.initUI)

        self.show()

    def initUI(self):
        self.resize(QSize(self.width, self.height))
        xcenter = self.x()
        self.move(xcenter,0)

    def set_recordbutton_tooltip(self, text):
        self.recordbutton.setToolTip(text)

    def clear_text_window(self):
        self.textbox.clear()

    def handleButton(self):
        if not self.recordbutton.is_recording:
            self.callbacks['start_recording']()
        else:
            self.callbacks['stop_recording']()

    # Make Window Draggable
    def mousePressEvent(self, event):
        self.oldPos = event.globalPos()
    def mouseMoveEvent(self, event):
        delta = QPoint (event.globalPos() - self.oldPos)
        self.move(self.x() + delta.x(), self.y() + delta.y())
        self.oldPos = event.globalPos()

    def insert_unfinished_text(self, newtext): self.textbox.insert_unfinished_text(newtext)
    def finalize_text(self, textprocessing_callback): self.textbox.finalize(textprocessing_callback)
    def get_current_text(self): return self.textbox.toPlainText()
    def show_recording_stopped(self): self.recordbutton.off()
    def show_recording_started(self): self.recordbutton.on()

    def set_callback(self, name, func):
        """
        See 'callback_registry.txt' for a full listing of available gui callbacks that can be set.
        """
        self.callbacks[name] = func


# Magic fairy runes stolen from StackOverflow
# https://stackoverflow.com/questions/10991991/pyside-easier-way-of-updating-gui-from-another-thread
# call invoke_in_main_thread(fn,*args, **kwargs) to interact with the GUI from outside the main thread
class InvokeEvent(QtCore.QEvent):
    EVENT_TYPE = QtCore.QEvent.Type(QtCore.QEvent.registerEventType())
    def __init__(self, fn, *args, **kwargs):
        QtCore.QEvent.__init__(self, InvokeEvent.EVENT_TYPE)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs

class Invoker(QtCore.QObject):
    def event(self, event):
        event.fn(*event.args, **event.kwargs)
        return True
_invoker = Invoker()
def invoke_in_main_thread(fn, *args, **kwargs):
    QtCore.QCoreApplication.postEvent(_invoker,
        InvokeEvent(fn, *args, **kwargs))
# END magic fairy runes block

def set_app_darktheme(app: PySide6.QtWidgets.QApplication) -> None:
    # https://stackoverflow.com/questions/48256772/dark-theme-for-qt-widgets
    app.setStyle("Fusion")
    # Now use a palette to switch to dark colors:
    palette = QPalette()
    palette.setColor(QPalette.Window, QColor(53, 53, 53))
    palette.setColor(QPalette.WindowText, Qt.white)
    palette.setColor(QPalette.Base, QColor(25, 25, 25))
    palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
    palette.setColor(QPalette.ToolTipBase, Qt.black)
    palette.setColor(QPalette.ToolTipText, Qt.white)
    palette.setColor(QPalette.Text, Qt.white)
    palette.setColor(QPalette.Button, QColor(53, 53, 53))
    palette.setColor(QPalette.ButtonText, Qt.white)
    palette.setColor(QPalette.BrightText, Qt.red)
    palette.setColor(QPalette.Link, QColor(42, 130, 218))
    palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
    palette.setColor(QPalette.HighlightedText, Qt.black)
    app.setPalette(palette)


app = QApplication(sys.argv)
set_app_darktheme(app)
def launch_gui() -> None: app.exec()
