import inspect
from typing import IO, Callable

class transcriber_revai:
    @staticmethod
    def response_is_final(response: dict, **kwargs) -> bool:
        raise NotImplementedError(f"{inspect.stack()[0][3]} Not Implemented.")

    @staticmethod
    async def transcribe_static(buffer: IO, api_key: str, **kwargs) -> dict:
        funcname = inspect.stack()[0][3]
        raise NotImplementedError(f"{funcname} Not Implemented.")

    @staticmethod
    async def transcribe_live(get_audio: Callable, api_key: str, channels:int, samplerate:int, returnvalues: dict, callback=print, **kwargs) -> None:
        from rev_ai.models import MediaConfig
        from rev_ai.streamingclient import RevAiStreamingClient
        try:
            example_mc = MediaConfig('audio/x-raw', 'interleaved', samplerate, 'S16LE', channels)
            streamclient = RevAiStreamingClient(api_key, example_mc)
            def audio_generator(): yield get_audio()
            response_gen = streamclient.start(audio_generator())
            for response in response_gen:
                callback(response)
        except Exception as e:
            print(e.with_traceback())
            returnvalues['error_value'] = 403

    @staticmethod
    def response_to_transcript(response: dict, response_index: int = 0) -> str:
        funcname = inspect.stack()[0][3]
        raise NotImplementedError(f"{funcname} Not Implemented.")