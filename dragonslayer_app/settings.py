import yaml

def read_settings():
    with open('settings.yaml') as fp:
        settings_dict = yaml.safe_load(fp)
    return settings_dict


def write_settings(settings_dict):
    with open('settings.yaml','w') as fp:
        yaml.safe_dump(settings_dict, fp)
