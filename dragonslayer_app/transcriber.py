import inspect
from typing import IO, Callable


class Transcriber:
    @staticmethod
    def response_is_final(response: dict, **kwargs) -> bool:
        raise NotImplementedError(f"{inspect.stack()[0][3]} Not Implemented.")

    @staticmethod
    async def transcribe_static(buffer: IO, api_key: str, **kwargs) -> dict:
        funcname = inspect.stack()[0][3]
        raise NotImplementedError(f"{funcname} Not Implemented.")

    @staticmethod
    async def transcribe_live(get_audio: Callable, api_key: str, channels:int, samplerate:int, returnvalues: dict, callback=print, **kwargs) -> None:
        funcname = inspect.stack()[0][3]
        raise NotImplementedError(f"{funcname} Not Implemented.")

    @staticmethod
    def response_to_transcript(response: dict, response_index: int = 0) -> str:
        funcname = inspect.stack()[0][3]
        raise NotImplementedError(f"{funcname} Not Implemented.")