import asyncio
import json
import queue
import sys
import threading
import time
import traceback as tb

import aiohttp.client_exceptions
import websockets
from typing import IO, Callable
from transcriber import Transcriber


class transcriber_deepgram(Transcriber):
    @staticmethod
    async def transcribe_static(buffer: IO, api_key, punctuate: bool = True, alternatives: int = 1, numerals: bool = True, model='nova-2') -> None:
        import deepgram # pip install deepgram-sdk
        """
        :param numerals:
        :param alternatives: int
        :param buffer: a file-like object containing bytes formatted as a .wav file, header included.
        :param punctuate: whether or not to auto-puncuate feature of deepgram
        :param model: 'general' or 'general-enhanced' or 'nova-2
        :return: dict, response data from server https://developers.deepgram.com/api-reference/
        """

        dg_client = deepgram.Deepgram(api_key)

        options = {'punctuate': punctuate, 'language': 'en', 'model': model, 'alternatives': alternatives,
                   'numerals': numerals}
        source = {'buffer': buffer, 'mimetype': 'audio'}
        try:
            response = await dg_client.transcription.prerecorded(source, options)
        except aiohttp.client_exceptions.ClientResponseError as e:
            print(e.with_traceback())
            response = None

        return response

    @staticmethod
    async def transcribe_live(get_audio: Callable, api_key:str, channels:int, samplerate:int, returnvalues: dict, callback=print, model:str='nova-2', **kwargs) -> None:
        """
        This is an implementation of 'transcribe_live' from transcriber.py
        :param get_audio: A function, takes 0 args and returns audio data
        :param channels: leave as 1 plz
        :param samplerate: should be 16000 or 44100 or the device default
        :param returnvalues: necessary to return error messages. Contains 4 keys, 'error_code', 'message', 'traceback', 'suggestion'
        :param callback: a function that runs whenever audio data is returned?
        :return:
        """
        try:
            async with websockets.connect(
                f'wss://api.deepgram.com/v1/listen?punctuate=false&channels={channels}&sample_rate={samplerate}&encoding=linear16&model={model}',
                extra_headers={
                    'Authorization': 'Token {}'.format(api_key)
                }
            ) as ws:
                async def sender(ws):
                    try:
                        while True:
                            data = get_audio()
                            await ws.send(data)
                            if data==b'':
                                break

                    except Exception as e:
                        print(f'Error while sending: {e}')
                        raise

                async def receiver(ws):
                    async for msg in ws:
                        res = json.loads(msg)
                        callback(res)

                await asyncio.wait([
                    asyncio.ensure_future(sender(ws)),
                    asyncio.ensure_future(receiver(ws))
                ])
        except Exception as e:
            type, value, traceback_object = sys.exc_info()
            returnvalues['error_code'] = type.__name__
            returnvalues['message'] = str(value)
            returnvalues['traceback'] = ''.join(tb.format_exception(traceback_object, value, traceback_object))


            if type.__name__=='InvalidStatusCode':
                returnvalues['suggestion'] = 'Check your deepgram API key in settings.yaml'
            elif 'getaddrinfo failed' in returnvalues['message']:
                returnvalues['suggestion'] = 'Check your internet connection.'
            else:
                returnvalues['suggestion'] = "Unrecognized Error, check the logfile at 'errorlog.txt' "

    @staticmethod
    def response_to_transcript(response: dict, response_index: int = 0) -> str:
        """
        :param response: dg response dict
        :param response_index: index of response, 0 by default, can attempt to get alternatives
        :return: string
        """

        if 'request_id' in response.keys(): return None # metadata only response.
        if 'is_final' in response.keys(): live = True
        else: live = False
        # https://developers.deepgram.com/api-reference/#transcription-prerecorded
        # https://developers.deepgram.com/api-reference/#transcription-streaming

        if live:
            return response['channel']['alternatives'][response_index]['transcript']
        elif not live:
            return response['results']['channels'][0]['alternatives'][response_index]['transcript']

    @staticmethod
    def response_is_final(response: dict) -> bool:
        if 'is_final' in response.keys(): return response['is_final']
        else: return True

    @staticmethod
    def response_to_dataframe(response: dict):
        import pandas as pd
        worddata = response['results']['channels'][0]['alternatives'][0]['words']
        confidence = [x['confidence'] for x in worddata]
        words = [x['word'] for x in worddata]
        start = [x['start'] for x in worddata]
        end = [x['end'] for x in worddata]
        lengthdata = [x['end'] - x['start'] for x in worddata]
        df = pd.DataFrame({'confidence': confidence, 'word': words,
                           'start': start, 'end': end, 'length_ms': lengthdata})
        return df


if __name__ == '__main__':
    def test_static():
        path = r'..\interview_speech-analytics.wav'
        with open(path, 'rb') as audio:
            response_dict = asyncio.run(transcriber_deepgram.transcribe_static(audio, alternatives=100))
        print(response_dict)
        transcript = response_dict['results']['channels'][0]['alternatives'][0]['transcript']
        print(transcript)
        saveto = path.replace('.wav', '.txt')
        with open(saveto, 'w+') as fp:
            fp.write(transcript)


    def schedule_stop_recording(live_q, stop_stream: callable):
        print('recording started')
        t1 = time.perf_counter()
        while True:
            t2 = time.perf_counter()
            if (t2 - t1) > 10:
                break
            else:
                time.sleep(1)
        live_q.put(b'')
        stop_stream()
        print('recording stopped')


    import pyaudio
    FRAMES_PER_BUFFER = 6400
    FORMAT = pyaudio.paInt16
    channels = 1
    rate = 44100

    p = pyaudio.PyAudio()
    deviceinfo = p.get_default_input_device_info()
    live_q = queue.Queue()

    def callback(in_data, frame_count, time_info, status):
        live_q.put(in_data)
        return (None, pyaudio.paContinue)

    # starts recording
    pyaudio_stream = p.open(
        format=FORMAT,
        channels=channels,
        rate=rate,
        input=True,
        output=False,
        frames_per_buffer=FRAMES_PER_BUFFER,
        stream_callback=callback
    )
    pyaudio_stream.start_stream()

    stopthread = threading.Thread(target=schedule_stop_recording, args=(live_q, pyaudio_stream.stop_stream))

    awaitable = transcriber_deepgram.transcribe_live(lambda: live_q.get(), channels=1, samplerate=rate, callback=print)
    asyncio_thread = threading.Thread(target = lambda: asyncio.run(awaitable))


    asyncio_thread.start()
    stopthread.start()

    asyncio_thread.join()
    stopthread.join()
    print('done')

