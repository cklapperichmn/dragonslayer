import time
from threading import Lock
from typing import List
import pyaudio as pa
import queue
from io import BytesIO
import soundfile as sf

class recordingdevice:
    def __init__(self, channels=1, samplerate=None, blocksize=6400, max_buffer_size_seconds=5):
        """
        :param channels:
        :param samplerate: 'None' to always use default samplerate
        :param blocksize:
        """
        self.p = pa.PyAudio()
        self.buffer = BytesIO()
        self.lock = Lock()
        self.live_qlist = []

        self.recordedaudio_q = queue.Queue()
        self.is_recording = False
        self.is_audio_formatted = True
        self.record_duration = 0
        self.devicelist = self.get_deviceinfo()
        self.channels = channels

        # dict_keys(['index', 'structVersion', 'name', 'hostApi', 'maxInputChannels', 'maxOutputChannels', 'defaultLowInputLatency', 'defaultLowOutputLatency', 'defaultHighInputLatency', 'defaultHighOutputLatency', 'defaultSampleRate'])
        self.deviceinfo = self.p.get_default_input_device_info()

        self.blocksize = blocksize
        self.samplerate = samplerate
        self.set_audio_device_by_id(self.p.get_default_input_device_info()['index'])
        self.max_livebuffer_samples = (self.get_samplerate()/blocksize) * max_buffer_size_seconds

    def get_deviceinfo(self) -> List[dict]:
        devices = [self.p.get_device_info_by_index(i) for i in range(self.p.get_device_count())]
        devices = [d for d in devices if d['maxInputChannels'] > 0]
        return devices

    def set_audio_device_by_name(self, name: str) -> None:
        with self.lock:
            if self.is_recording:
               raise ValueError("can't change device while recording!")
        devices = [d for d in self.devicelist if d['name']==name]
        self.set_audio_device_by_id(devices[0]['index'])

    def set_audio_device_by_id(self, deviceid: int):
        with self.lock:
            if self.is_recording:
               raise ValueError("can't change device while recording!")
            devices = [d for d in self.devicelist if d['index']==deviceid]
            self.deviceinfo = devices[0]
            self.devicename = self.deviceinfo['name']

    def get_formatted_audio_buffer(self) -> BytesIO:
        """
        WARNING: DO NOT USE FOR LIVE RECORDING
        WARNING: THIS REPLACES THE BUFFER YOU GET FROM START_AUDIO WITH A FORMATTED BUFFER
        :return: A wav-formatted audio buffer w/ file header
        """
        with self.lock:
            if self.is_recording:
                raise ValueError("please stop recording before calling get_audio()")

            if self.is_audio_formatted:
                return self.buffer

            with sf.SoundFile(self.buffer, format='WAV', mode='w+', samplerate=self.get_samplerate(), channels=self.channels,
                              subtype='PCM_16') as file:
                while not self.recording_queue.empty():
                    file.write(self.recording_queue.get())
            self.buffer.seek(0)
            self.is_audio_formatted = True
        return self.buffer

    def get_duration_seconds(self) -> float:
        return self.record_duration

    def stopaudio(self) -> None:
        """
        Stop a recording. If called twice, or called while not recording: does nothing & returns nothing with no error.
        :return:
        """
        with self.lock:
            if not self.is_recording: return
            self.inputstream.stop_stream()
            self.record_duration = time.perf_counter() - self._recordingstart
            for q in self.live_qlist: q.put(b'')
            self.is_recording = False


    def get_samplerate(self):
        return self.samplerate if self.samplerate else int(self.deviceinfo['defaultSampleRate'])

    def startaudio(self, n=1) -> BytesIO:
        """
        Start a recording. If called twice, returns nothing and does nothing with no error.
        :return: a buffer of raw audio bytes that gets written to continuously in real-time
        """
        self.live_qlist = [queue.Queue(maxsize=self.max_livebuffer_samples) for _ in range(n)]
        with self.lock:
            if self.is_recording: return
            self.buffer = BytesIO()
            self.record_duration = 0

            # define callback (2)
            def callback(in_data, frame_count, time_info, status):
                for q in self.live_qlist:
                    q.put(in_data)
                    if q.full(): q.get()

                self.recordedaudio_q.put(in_data)
                return (None, pa.paContinue)

            self.inputstream = self.p.open(
                format=pa.paInt16,
                channels=self.channels,
                rate=self.get_samplerate(),
                input=True,
                frames_per_buffer=self.blocksize,
                input_device_index=self.deviceinfo['index'],
                stream_callback=callback,
            )

            self._recordingstart = time.perf_counter()
            self.is_recording = True
            self.is_audio_formatted = False

        return self.live_qlist


if __name__ == '__main__':
    device = recordingdevice()
    device.stopaudio()
    device.stopaudio()
    device.startaudio()
    device.startaudio()
    device.startaudio()
    device.startaudio()
    device.stopaudio()
    device.stopaudio()
    audio = device.get_formatted_audio_buffer()
    print(len(audio.read()))
    print('done')
