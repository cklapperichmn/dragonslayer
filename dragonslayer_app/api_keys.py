import yaml
def get_revai_api_key():
    with open('settings.yaml', 'r') as fp:
        settings = yaml.safe_load(fp)
    return settings['REV_API_KEY']
def get_deepgram_api_key():
    with open('settings.yaml', 'r') as fp:
        settings = yaml.safe_load(fp)
    return settings['DEEPGRAM_API_KEY']
def get_assemblyai_api_key():
    with open('settings.yaml', 'r') as fp:
        settings = yaml.safe_load(fp)
    return settings['ASSEMBLY_API_KEY']