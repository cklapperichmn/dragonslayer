import queue
import websockets
import asyncio
import base64
import json
import inspect
from typing import IO, Callable

import yaml


class transcriber_assemblyai:
    @staticmethod
    def response_is_final(response: dict, **kwargs) -> bool:
        return response['message_type'] == 'FinalTranscript'

    @staticmethod
    async def transcribe_static(buffer: IO, api_key: str, **kwargs) -> dict:
        funcname = inspect.stack()[0][3]
        raise NotImplementedError(f"{funcname} Not Implemented.")

    @staticmethod
    async def transcribe_live(get_audio: Callable, api_key: str, channels:int, samplerate:int, returnvalues: dict, callback=print, **kwargs) -> None:
        try:
            loop = asyncio.get_running_loop()
            samplerate = int(samplerate)

            URL = f"wss://api.assemblyai.com/v2/realtime/ws?sample_rate={samplerate}?channels={channels}"
            print(f'Connecting websocket to url ${URL}')

            async with websockets.connect(
                    URL,
                    extra_headers=(("Authorization", api_key),),
                    ping_interval=5,
                    ping_timeout=20
            ) as _ws:
                await asyncio.sleep(0.1)
                print("Receiving SessionBegins ...")
                session_begins = await _ws.recv()
                stopped = False
                print(session_begins)
                print("Sending messages ...")

                async def send():
                    nonlocal stopped
                    while True:
                        try:
                            data = get_audio()
                            if data == b'':
                                stopped = True
                                break

                            data = base64.b64encode(data).decode("utf-8")
                            json_data = json.dumps({"audio_data": str(data)})
                            await _ws.send(json_data)

                        except websockets.exceptions.ConnectionClosedError as e:
                            print(e)
                            assert e.code == 4008
                            break
                        except OSError as e:
                            if not e.errno == -9983: raise e
                            break
                        await asyncio.sleep(0.01)
                    return True

                async def receive():
                    nonlocal stopped
                    while True:
                        try:
                            result_str = await _ws.recv()
                            response = json.loads(
                                result_str)  # dict_keys(['audio_start', 'audio_end', 'confidence', 'text', 'words', 'created', 'message_type', 'punctuated', 'text_formatted'])
                            loop.call_soon_threadsafe(callback, response)
                            if stopped and response['message_type'] == 'FinalTranscript':
                                break
                        except websockets.exceptions.ConnectionClosedError as e:
                            print(e)
                            assert e.code == 4008
                            break

                await asyncio.gather(send(), receive())

        except websockets.exceptions.InvalidStatusCode:
            print('exception')
            returnvalues['error_code']=403

    @staticmethod
    def response_to_transcript(response: dict, response_index: int = 0) -> str:
        return response['text_formatted']

if __name__ == '__main__':
    import pyaudio
    FRAMES_PER_BUFFER = 6400
    FORMAT = pyaudio.paInt16
    channels = 1
    rate = 16000

    p = pyaudio.PyAudio()
    deviceinfo = p.get_default_input_device_info()

    results_q = queue.Queue()
    recordedaudio_q = queue.Queue()
    live_q = queue.Queue()

    def callback(in_data, frame_count, time_info, status):
        live_q.put(in_data)
        print(in_data)
        recordedaudio_q.put(in_data)
        return (None, pyaudio.paContinue)

    # starts recording
    pyaudio_stream = p.open(
        format=FORMAT,
        channels=channels,
        rate=rate,
        input=True,
        frames_per_buffer=FRAMES_PER_BUFFER,
        stream_callback=callback
    )

    returnvalues = {}
    with open('settings.yaml', 'r') as fp:
        settings = yaml.safe_load(fp)

    apikey = settings['ASSEMBLY_API_KEY']
    awaitable = transcriber_assemblyai.transcribe_live(live_q, api_key=apikey, channels=1,
                                                       samplerate=16000, returnvalues=returnvalues, callback=print)
    asyncio.run(awaitable)
