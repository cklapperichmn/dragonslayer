pip install -r requirements.txt

cd dragonslayer_app
pyinstaller "dragonslayer_app.py" --onefile
cd ..

del dragonslayer /Q
mkdir dragonslayer
mkdir "dragonslayer/assets"
copy "dragonslayer_app/assets" "dragonslayer/assets"
copy "default_settings.yaml" "dragonslayer/settings.yaml"
copy "dragonslayer_app/dist/dragonslayer_app.exe" "dragonslayer/dragonslayer_app.exe"
copy README.MD dragonslayer

cd dragonslayer
tar.exe -a -c -f dragonslayer.zip
cd ..