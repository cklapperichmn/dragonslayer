import time
from rapidfuzz import fuzz
import sys
import os
sys.path.append(os.path.dirname(__file__))
from __simplewords import simplewords
import english_words
full_english_dictionary = english_words.english_words_lower_set
import numpy as np

tokens_to_strip = '.?#!-, '


def predict_incomplete_word_naive(input_text, num_predictions=5, dictionary='simple', method='rapidfuzz'):
    """

    :param method: 'rapidfuzz'
    :param input_text:
    :param num_predictions:
    :param dictionary: 'simple' or 'full'
    :return:
    """

    if dictionary == 'simple':
        wordlist = simplewords
    elif dictionary == 'full':
        wordlist = full_english_dictionary
    else:
        raise ValueError

    partialword = input_text.split(' ')[-1]
    partialword = partialword.strip(tokens_to_strip).lower()
    substr_len = len(partialword)+1

    if method == 'rapidfuzz':
        distances = [(word, fuzz.ratio(partialword, word[:substr_len])) for word in wordlist]
        words = sorted(distances, key=lambda x: x[1], reverse=True)
    else:
        raise ValueError

    words = words[:num_predictions]
    words = [w[0] for w in words] # we only want the word not the score
    return words


if __name__ == '__main__':
    t1 = time.perf_counter()
    print(predict_incomplete_word_naive('hello, worl', 5))
    t2 = time.perf_counter()
    print(t2-t1)