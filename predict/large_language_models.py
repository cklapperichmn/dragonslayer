import time

from transformers import T5Config, T5ForConditionalGeneration, T5Tokenizer
# https://huggingface.co/blog/constrained-beam-search
class word_predictor_t5:
    def __init__(self):
        model_name = "allenai/t5-small-next-word-generator-qoogle"
        self.tokenizer = T5Tokenizer.from_pretrained(model_name)
        self.model = T5ForConditionalGeneration.from_pretrained(model_name)

    def get_next_word_predictions(self, input_string, num_predictions=5, prediction_max_length=1, **generator_args):
        prediction_max_length += len(input_string)
        input_ids = self.tokenizer.encode(input_string, return_tensors="pt")

        sample_outputs = self.model.generate(
        input_ids,
        do_sample=True,
        max_length=prediction_max_length,
        top_k=50,
        top_p=0.95,
        num_return_sequences=num_predictions)

        output = self.tokenizer.batch_decode(sample_outputs, skip_special_tokens=True)
        return output


if __name__ == '__main__':
    predictor = word_predictor_t5()
    t1 = time.perf_counter()
    print(predictor.get_next_word_predictions("hello, "))
    t2 = time.perf_counter()
    print(t2-t1)