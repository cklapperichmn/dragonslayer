import time

from transformers import T5Config, T5ForConditionalGeneration, T5Tokenizer, GPT2LMHeadModel, GPT2Tokenizer

# https://huggingface.co/blog/constrained-beam-search
tokens_to_strip = '.?#!-, '

class word_predictor_t5:
    def __init__(self, model='distilgpt2'):
        if model=='t5':
            model_name = "allenai/t5-small-next-word-generator-qoogle"
            self.tokenizer = T5Tokenizer.from_pretrained(model_name)
            self.model = T5ForConditionalGeneration.from_pretrained(model_name)

        elif model=='distilgpt2':
            model_name='distilgpt2'
            self.tokenizer = GPT2Tokenizer.from_pretrained(model_name)
            self.model = GPT2LMHeadModel.from_pretrained(model_name)

    def get_next_word_predictions(self, input_string, num_predictions=5, prediction_max_length=1, vocab=None, **generator_args):
        prediction_max_length += len(input_string)
        input_ids = self.tokenizer.encode(input_string, return_tensors="pt")
        if vocab:
            force_words_ids = self.tokenizer(vocab, add_special_tokens=False).input_ids
        else:
            force_words_ids = None
        force_words_ids = [c[0] for c in force_words_ids if len(c) == 1]
        # self.tokenizer.decode([c[0] for c in force_words_ids])
        num_beams = max(len(vocab), num_predictions)
        num_beams = min(num_beams, 5)
        output = self.model.generate(
            input_ids,
            do_sample=False,
            num_beams=10,
            force_words_ids=[[force_words_ids]],
            max_new_tokens=5,
            num_return_sequences=5,
            no_repeat_ngram_size=1,
            temperature=.85,
            top_k=25,
            remove_invalid_values=True,
            early_stopping=True)

        output_text = self.tokenizer.batch_decode(output, skip_special_tokens=True)
        print(output_text)
        return output_text

if __name__ == '__main__':
    predictor = word_predictor_t5(model='t5')
    input_text = 'hello this is a test sent'

    splitsentence = input_text.split(' ')
    modelinputtext = splitsentence[0].strip()
    incompleteword = splitsentence[-1].strip(tokens_to_strip)
    print(modelinputtext, incompleteword)

    from distancebased import predict_incomplete_word_naive
    closestwords = predict_incomplete_word_naive(input_text=incompleteword, num_predictions=100, dictionary='full')
    t1 = time.perf_counter()
    print(predictor.get_next_word_predictions(input_string=modelinputtext, num_predictions=5, vocab=closestwords))
    t2 = time.perf_counter()
    print(t2-t1)