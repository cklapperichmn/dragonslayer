import sys
import traceback

def raisesexception():
    raise ValueError('whoops')
try:
    raisesexception()
except Exception as e:
    exception_class, exception, tb = sys.exc_info()
    traceback.print_exception(e)