import asyncio
import threading
import time
from tkinter import Scrollbar, Tk, RIGHT, Button, Y, DISABLED, NORMAL, Label
import pyperclip
import keyboard
from tkinter import Text, _tkinter
import deepgram_transcribe as dg
import textprocessing
import settings
import recordingdevice_pa as recordingdevice
import hotkey_activator

hotkey_name = settings.settings['recording_hotkey']

def show_filedialogue(initialdir):
    import tkinter as tk
    from tkinter import filedialog
    root = tk.Tk()
    root.withdraw()
    root.wm_attributes('-topmost', 1)
    filetypes = [('mp3','*.mp3'),('mp4','*.mp4')]
    file_path = filedialog.askopenfilename(initialdir=initialdir,filetypes=filetypes, parent=root)
    return file_path


class CustomText(Text):
    # source: https://stackoverflow.com/questions/40617515/python-tkinter-text-modified-callback
    def __init__(self, *args, **kwargs):
        """A text widget that report on internal widget commands"""
        Text.__init__(self, *args, **kwargs)

        # create a proxy for the underlying widget
        self._orig = self._w + "_orig"
        self.tk.call("rename", self._w, self._orig)
        self.tk.createcommand(self._w, self._proxy)

    def _proxy(self, command, *args):
        cmd = (self._orig, command) + args
        try:
            result = self.tk.call(cmd)
        except _tkinter.TclError:
            return None

        if command in ("insert", "delete", "replace"):
            self.event_generate("<<TextModified>>")

        return result

def get_tkinter_gui():
    recorder = recordingdevice.recordingdevice(channels=1)
    lock = threading.Lock()

    def startaudio():
        with lock:
            recorder.startaudio()
            activatebutton['state'] = DISABLED
            stopbutton['state'] = NORMAL

    def stopaudio():
        with lock:
            activatebutton['state'] = NORMAL
            stopbutton['state'] = DISABLED
            recorder.stopaudio()

            if recorder.record_duration < 0.25:
                """clear and paste"""
                keyboard.press_and_release(settings.settings['tap-command'])
                time.sleep(0.05)
                keyboard.press_and_release('enter')
                text_info.delete(1.0, "end")

            else:
                """format audio, transcribe, format text, append new text """
                buffer = recorder.get_formatted_audio_buffer()
                # this automatically closes the buffer when done!!
                awaitable = dg.transcribe_static(buffer, punctuate=settings.settings['auto-punctuate'])
                response = asyncio.run(awaitable)
                text = dg.response_to_transcript(response, live=False)
                text = textprocessing.parse_commands(text)
                text = textprocessing.capitalize(text)

                previous_text = text_info.get(1.0, 'end-1c')
                newtext = previous_text + ' ' + text
                newtext = textprocessing.fix_spaces_around_punctuation(newtext)

                text_info.delete(1.0, "end")
                text_info.insert(1.0, newtext)

    root = Tk()
    root.geometry("500x1200")
    root.title("dragonslayer")
    root.minsize(height=500, width=700)
    root.maxsize(height=500, width=700)
    if settings.settings['keep-window-on-top']:
        root.wm_attributes('-topmost', 1)

    hotkey_activator.set_hotkey_bindings(onpress=startaudio, onrelease=stopaudio, hotkey=hotkey_name)

    # adding scrollbar
    scrollbar = Scrollbar(root)

    # packing scrollbar
    scrollbar.pack(side=RIGHT, fill=Y)


    activatebutton = Button(root, text="RECORD", command=startaudio)
    activatebutton.pack()

    stopbutton = Button(root, text="STOP", command=stopaudio)
    stopbutton.pack()

    devicename_text = Label(root, text=f"{recorder.devicename}")
    devicename_text.pack()

    # configuring the scrollbar
    text_info = CustomText(root, yscrollcommand=scrollbar.set)
    text_info.pack()
    text_info.bind('<<TextModified>>', lambda event: pyperclip.copy(event.widget.get("1.0", "end-1c")))

    scrollbar.config(command=text_info.yview)

    return root


if __name__ == '__main__':
    root = get_tkinter_gui()
    root.mainloop()
