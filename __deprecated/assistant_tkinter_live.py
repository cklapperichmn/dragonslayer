import asyncio
import threading
from tkinter import Scrollbar, Tk, RIGHT, Button, Y, DISABLED, NORMAL, Label, INSERT, Text
import pyperclip
import keyboard

import deepgram_transcribe as dg
import textprocessing
import settings
import recordingdevice_pa as recordingdevice
import hotkey_activator

hotkey_name = settings.settings['recording_hotkey']


def get_tkinter_gui():
    recorder = recordingdevice.recordingdevice(channels=1)
    lock = threading.Lock()

    def get_textbox_update_callback():
        unfinished_text = ''
        unfinishedtext_startposition = text_info.index(INSERT)
        def update_textbox(response):
            nonlocal unfinished_text
            nonlocal unfinishedtext_startposition

            newtext = dg.response_to_transcript(response, live=True)
            if not newtext:return
            newtext = ' ' + textprocessing.parse_commands(newtext) + ' '

            end_index = f'{unfinishedtext_startposition} + {len(unfinished_text)} chars'
            text_info.delete(unfinishedtext_startposition, end_index)
            text_info.insert(unfinishedtext_startposition, newtext)
            unfinished_text = newtext

            if dg.response_is_final(response): # response['message_type'] == 'FinalTranscript':
                unfinished_text = ''
                unfinishedtext_startposition = text_info.index(INSERT)
                fulltext = text_info.get("1.0",'end-1c')
                fulltext = textprocessing.fix_spaces_around_punctuation(fulltext)
                fulltext = textprocessing.capitalize(fulltext)
                text_info.delete('1.0','end-1c')
                text_info.insert('1.0', fulltext)
        return update_textbox

    transcription_thread = None
    def startaudio():
        with lock:
            nonlocal transcription_thread
            live_queue = recorder.startaudio()
            activatebutton['state'] = DISABLED
            stopbutton['state'] = NORMAL

            textbox_responsehandler = get_textbox_update_callback()
            awaitable = dg.transcribe_live(lambda: live_queue.get(), channels=1, samplerate=recorder.get_samplerate(), callback=textbox_responsehandler)
            transcription_thread = threading.Thread(target=asyncio.run, args=(awaitable,))
            transcription_thread.start()

    def stopaudio():
        nonlocal transcription_thread
        with lock:
            activatebutton['state'] = DISABLED
            stopbutton['state'] = DISABLED
            recorder.stopaudio()

            if recorder.record_duration < 0.25:
                """clear and paste"""
                text =text_info.get("1.0",'end-1c')
                pyperclip.copy(text)
                keyboard.press_and_release('ctrl+v')
            if transcription_thread: transcription_thread.join()
            transcription_thread = None
            activatebutton['state'] = NORMAL

    root = Tk()
    root.geometry("500x1200")
    root.title("dragonslayer")
    root.minsize(height=500, width=700)
    root.maxsize(height=500, width=700)
    if settings.settings['keep-window-on-top']:
        root.wm_attributes('-topmost', 1)

    hotkey_activator.set_hotkey_bindings(onpress=startaudio, onrelease=stopaudio, hotkey=hotkey_name)

    scrollbar = Scrollbar(root)
    scrollbar.pack(side=RIGHT,fill=Y)

    activatebutton = Button(root, text="RECORD", command=startaudio)
    activatebutton.pack()

    stopbutton = Button(root, text="STOP", command=stopaudio)
    stopbutton.pack()

    devicename_text = Label(root, text=f"{recorder.devicename}")
    devicename_text.pack()

    text_info = Text(root, yscrollcommand=scrollbar.set)
    text_info.pack()

    scrollbar.config(command=text_info.yview)

    return root


if __name__ == '__main__':
    root = get_tkinter_gui()
    root.mainloop()
