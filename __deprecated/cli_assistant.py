import asyncio
import deepgram_transcribe, transcribe_live
from recordingdevice import recordingdevice
import time
import hotkey_activator

recorder = recordingdevice()

hotkey_name = 'ctrl+space'

def onpress():
    print('RECORDING')
    recorder.startaudio()

def onrelease():
    print('FINISHED')
    recorder.stopaudio()
    buffer = recorder.get_audio()
    awaitable = deepgram_transcribe.dg_transcribe_static(buffer)
    response = asyncio.run(awaitable)
    text = deepgram_transcribe.dg_response_to_transcript(response)
    print(text)


if __name__=='__main__':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    hotkey_activator.set_hotkey_bindings(onpress, onrelease, hotkey=hotkey_name)
    while(True):
        time.sleep(0.05)